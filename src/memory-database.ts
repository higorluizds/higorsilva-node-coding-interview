import { DatabaseInstanceStrategy } from './database'

type DbProps = {
    test: boolean
}

export const db = async (props: DbProps) => {
    if (props.test) return DatabaseInstanceStrategy.setInstanceByEnv('test')
}
