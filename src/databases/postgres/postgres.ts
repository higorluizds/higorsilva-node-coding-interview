import { Database } from '../database_abstract'

import { newDb, IMemoryDb } from 'pg-mem'

export class PostgreStrategy extends Database {
    _instance: IMemoryDb

    constructor() {
        super()
        this.getInstance()
    }

    private async getInstance() {
        const db = newDb()

        db.public.many(`
            CREATE TABLE flights (
                code VARCHAR(5) PRIMARY KEY,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );

            CREATE TABLE persons (
                id int(12) auto_crement PRIMARY KEY,
                name VARCHAR(255),
                gender enum('male', 'female', 'other'),
                email VARCHAR(50)
            );

            CREATE TABLE passengers (
                id int(12) auto_crement PRIMARY KEY,
                flight_code VARCHAR(5),
                person_id int(12)
            );
        `)

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')


            INSERT INTO persons (name, gender, email)
            VALUES ('John Smith"', 'male', 'ohn.smith@email.com'),
                     ('Matt Simons', 'male', 'matt@email.com')
        `)

        PostgreStrategy._instance = db

        return db
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights')
    }

    public async getPersons() {
        return PostgreStrategy._instance.public.many('SELECT * FROM persons')
    }

    public async updateFlightStatus(
        code: string,
        flight: {
            origin: string
            destination: string
            status: string
        }
    ) {
        return PostgreStrategy._instance.public.many(`
            UPDATE flights
                set origin = '${flight.origin}',
                    destination = '${flight.destination}',
                    status = '${flight.status}'
                WHERE code = '${code}'
        `)
    }

    public async addFlight(flight: {
        code: string
        origin: string
        destination: string
        status: string
    }) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`
        )
    }
}
