import BaseModel from './core/BaseModel'

export default class PersonsModel extends BaseModel {
    public async getPersons() {
        return await this._db.getPersons()
    }
}
