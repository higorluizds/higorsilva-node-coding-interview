import { Database } from '../../databases/database_abstract'
import { DatabaseInstanceStrategy } from '../../database'

export default class BaseModel {
    public table: string
    public readonly _db: Database

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
    }
}
