import PersonModel from '../models/persons.model'

export class PersonsService {
    public model = new PersonModel()

    public async getPersons() {
        return await this.model.getPersons()
    }
}
